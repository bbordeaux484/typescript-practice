# TypeScript Practice

## Name
Brandy's TypeScript Practice Playground to Strengthen my Programing Abilities

## Description
This space is my trying out solutions to different typescript training prompts I find online.

## Usage
This repo is meant for viewing purposes only.

## Support
There will be no support available for these projects.

## Contributing
I do not accept contributors as this is a solo practice space for me to develop my skills as a typescript developer.

## Project status
This project will be on going until I switch to a new skill to work on.
